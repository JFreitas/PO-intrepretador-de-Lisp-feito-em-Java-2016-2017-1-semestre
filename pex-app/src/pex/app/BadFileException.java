package pex.app;

import pt.tecnico.po.ui.DialogException;

/**
 * Exception for representing file errors.
 */
@SuppressWarnings("nls")
public class BadFileException extends DialogException {

    /** Serial number for serialization. */
    private static final long serialVersionUID = 201608241029L;

    /** Original leaf expression. */
    String _fileName;

    /**
     * @param position
     */
    public BadFileException(String fileName) {
        _fileName = fileName;
    }

    /**
     * @return the position
     */
    public String getFileName() {
        return _fileName;
    }

    /** @see pt.tecnico.po.ui.DialogException#getMessage() */
    @Override
    public String getMessage() {
        return "Ficheiro inválida: " + _fileName;
    }

}