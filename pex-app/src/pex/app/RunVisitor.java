package pex.app;

import pex.Value;
import pex.atomic.Identifier;
import pex.atomic.IntegerLiteral;
import pex.atomic.StringLiteral;
import pex.operators.Add;
import pex.operators.Mul;
import pex.operators.Not;
import pex.operators.Neg;
import pex.operators.Sub;
import pex.operators.Div;
import pex.operators.Mod;
import pex.operators.Lt;
import pex.operators.Le;
import pex.operators.Ge;
import pex.operators.Gt;
import pex.operators.Eq;
import pex.operators.Ne;
import pex.operators.And;
import pex.operators.Or;
import pex.operators.Set;
import pex.operators.Print;
import pex.operators.Readi;
import pex.operators.Reads;
import pex.operators.If;
import pex.operators.While;
import pex.operators.Call;
import pex.operators.Program;
import pex.operators.Sequence;
import pex.Visitor;
import pex.Expression;
import pex.Interpreter;

import java.util.Collections;
import java.util.List;
import java.util.HashMap;

import pt.tecnico.po.ui.Form;
import pt.tecnico.po.ui.Display;
import pt.tecnico.po.ui.Input;

public class RunVisitor implements Visitor {
    public RunVisitor(Interpreter interpreter){
        _interpreter = interpreter;
    }

    private Interpreter _interpreter;
    private Form _formI = new Form();
    private Form _formS = new Form();
    private Display _display = new Display();

    private Input<Integer> _inputInteger = _formI.addIntegerInput("Introduza um inteiro:");
    private Input<String> _inputString = _formS.addStringInput("Introduza uma cadeia de caracteres:");

    private Value _value;
    private Identifier _identifier;
    private int _line = 0;
    private HashMap<String,Value> _map = new HashMap<String,Value>();

    public void visitIdentifier(Identifier i) {
        _identifier = i;
        Value v = _map.get(i.getName());
        if (v == null){
            _value = new IntegerLiteral(0);
        }
        else{
            _value = v;
        }

    }

    public void visitSet(Set e){
        try {
            e.first().accept(this);
            Identifier i = (Identifier) _identifier;
            e.second().accept(this);
            _map.put(i.getName(),_value);
        } catch (RuntimeException err) {
            String s = "Erro de execução na expressão set na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitIntegerLiteral(IntegerLiteral i) {
        _value = i;
    }

    public void visitStringLiteral(StringLiteral s) {
        _value = s;
    }

    public void visitAdd(Add e){
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            e.second().accept(this);
            IntegerLiteral i2 = (IntegerLiteral) _value;
            Integer result = i1.getValue() + i2.getValue();
            _value = new IntegerLiteral(result);
        }
        catch (RuntimeException err){
            String s = "Erro de execução na expressão add na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitMul(Mul e){
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            e.second().accept(this);
            IntegerLiteral i2 = (IntegerLiteral) _value;
            Integer result = i1.getValue() * i2.getValue();
            _value = new IntegerLiteral(result);
        }
        catch (RuntimeException err){
            String s = "Erro de execução na expressão mul na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitNot(Not e){
        try {
            e.argument().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            Integer result;
            if (i1.getValue() == 0){
                result = 1;
            }
            else{
                result = 0;
            }
            _value = new IntegerLiteral(result);
        }
        catch (RuntimeException err){
            String s = "Erro de execução na expressão not na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitNeg(Neg e){
        try {
            e.argument().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            Integer result = i1.getValue() * -1;
            _value = new IntegerLiteral(result);
        }
        catch (RuntimeException err){
            String s = "Erro de execução na expressão neg na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitSub(Sub e){
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            e.second().accept(this);
            IntegerLiteral i2 = (IntegerLiteral) _value;
            Integer result = i1.getValue() - i2.getValue();
            _value = new IntegerLiteral(result);
        }
        catch (RuntimeException err){
            String s = "Erro de execução na expressão sub na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitDiv(Div e){
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            e.second().accept(this);
            IntegerLiteral i2 = (IntegerLiteral) _value;
            Integer result = i1.getValue() / i2.getValue();
            _value = new IntegerLiteral(result);
        }
        catch (RuntimeException err){
            String s = "Erro de execução na expressão div na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitMod(Mod e){
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            e.second().accept(this);
            IntegerLiteral i2 = (IntegerLiteral) _value;
            Integer result = i1.getValue() % i2.getValue();
            _value = new IntegerLiteral(result);
        }
        catch (RuntimeException err){
            String s = "Erro de execução na expressão mod na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitLt(Lt e){
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            e.second().accept(this);
            IntegerLiteral i2 = (IntegerLiteral) _value;
            Integer result;
            if (i1.getValue() < i2.getValue()){
                result = 1;
            }
            else{
                result = 0;
            }
            _value = new IntegerLiteral(result);
        }
        catch (RuntimeException err){
            String s = "Erro de execução na expressão lt na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitLe(Le e) {
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            e.second().accept(this);
            IntegerLiteral i2 = (IntegerLiteral) _value;
            Integer result;
            if (i1.getValue() <= i2.getValue()) {
                result = 1;
            }
            else {
                result = 0;
            }
            _value = new IntegerLiteral(result);
        } catch (RuntimeException err) {
            String s = "Erro de execução na expressão le na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitGe(Ge e){
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            e.second().accept(this);
            IntegerLiteral i2 = (IntegerLiteral) _value;
            Integer result;
            if (i1.getValue() >= i2.getValue()) {
                result = 1;
            }
            else {
                result = 0;
            }
            _value = new IntegerLiteral(result);
        } catch (RuntimeException err) {
            String s = "Erro de execução na expressão ge na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitGt(Gt e){
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            e.second().accept(this);
            IntegerLiteral i2 = (IntegerLiteral) _value;
            Integer result;
            if (i1.getValue() > i2.getValue()) {
                result = 1;
            }
            else {
                result = 0;
            }
            _value = new IntegerLiteral(result);
        } catch (RuntimeException err) {
            String s = "Erro de execução na expressão gt na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitEq(Eq e){
        try {
        e.first().accept(this);
        IntegerLiteral i1 = (IntegerLiteral) _value;
        e.second().accept(this);
        IntegerLiteral i2 = (IntegerLiteral) _value;
        Integer result;
        if (i1.getValue() == i2.getValue()) {
            result = 1;
        }
        else {
            result = 0;
        }
        _value = new IntegerLiteral(result);
         } catch (RuntimeException err) {
            String s = "Erro de execução na expressão eq na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitNe(Ne e){
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            e.second().accept(this);
            IntegerLiteral i2 = (IntegerLiteral) _value;
            Integer result;
            if (i1.getValue() != i2.getValue()) {
                result = 1;
            }
            else {
                result = 0;
            }
            _value = new IntegerLiteral(result);
        } catch (RuntimeException err) {
            String s = "Erro de execução na expressão ne na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitAnd(And e){
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            Integer result;
            if (i1.getValue() == 0) {
                result = 0;
            }
            else{
                e.second().accept(this);
                IntegerLiteral i2 = (IntegerLiteral) _value;
                if(i2.getValue() == 0){
                    result = 0;
                }
                else{
                    result = 1;
                }
            }
            _value = new IntegerLiteral(result);
        } catch (RuntimeException err) {
            String s = "Erro de execução na expressão and na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitOr(Or e){
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            Integer result;
            if (i1.getValue() != 0) {
                result = 1;
            }
            else{
                e.second().accept(this);
                IntegerLiteral i2 = (IntegerLiteral) _value;
                if(i2.getValue() != 0){
                    result = 1;
                }
                else{
                    result = 0;
                }
            }
            _value = new IntegerLiteral(result);
        } catch (RuntimeException err) {
            String s = "Erro de execução na expressão or na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }




    public void visitReadi(Readi r){
        try {
            _formI.parse();
            _value = new IntegerLiteral(_inputInteger.value());
        } catch (RuntimeException err) {
            String s = "Erro de execução na expressão readi na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitReads(Reads r){
        try {
            _formS.parse();
            _value = new StringLiteral(_inputString.value());
        } catch (RuntimeException err) {
            String s = "Erro de execução na expressão reads na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }


    public void visitIf(If e){
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            if(i1.getValue() == 0){
                e.third().accept(this);
            }
            else{
                e.second().accept(this);
            }
        } catch (RuntimeException err) {
            String s = "Erro de execução na expressão if na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitWhile(While e){
        try {
            e.first().accept(this);
            IntegerLiteral i1 = (IntegerLiteral) _value;
            while(i1.getValue() != 0){
                e.second().accept(this);
                e.first().accept(this);
                i1 = (IntegerLiteral) _value;
            }
            _value = i1;
        } catch (RuntimeException err) {
            String s = "Erro de execução na expressão or na linha: " + _line;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitCall(Call e){
        try {
            e.argument().accept(this);
            StringLiteral i1 = (StringLiteral) _value;
            _interpreter.getProgram(i1.getValue()).accept(this);
        }
        catch (RuntimeException err){
            String s = "Erro de execução na expressão call na linha: " + _line ;
            _display = new Display();
            _display.popup(s);
            throw err;
        }
    }

    public void visitProgram(Program p){
        if(p.getAll().size() == 0){
            _value = new IntegerLiteral(0);
        }
        else {
            for (Expression e : p.getAll()) {
                e.accept(this);
                _line++;
            }
        }
    }

    public void visitSequence(Sequence p){
        if(p.getAll().size() == 0){
            _value = new IntegerLiteral(0);
        }
        else {
            for (Expression e : p.getAll()) {
                e.accept(this);
            }
        }
    }

    public void visitPrint(Print p){
        String print = "";
        for (Expression e : p.getAll()) {
            e.accept(this);
            print += _value.toString();
        }
        print += "\n";
        _display = new Display();
        _display.popup(print);
    }

}
