/** @version $Id: DoRunProgram.java,v 1.5 2016/12/09 09:21:51 ist424781 Exp $ */
package pex.app.evaluator;

import pex.Interpreter;
import pex.operators.Program;
import pex.app.RunVisitor;
import java.lang.ClassCastException;

/**
 * Run program.
 */
public class DoRunProgram extends ProgramCommand {

  /**
   * @param interpreter
   * @param receiver
   */
  public DoRunProgram(Interpreter interpreter, Program receiver) {
    super(Label.RUN_PROGRAM, interpreter, receiver);
  }

  /**
   * @see pt.tecnico.po.ui.Command#execute()
   */
  @Override
  public final void execute() {
    try {
      RunVisitor runVisitor = new RunVisitor(_interpreter);
      _receiver.accept(runVisitor);
    } catch (RuntimeException err) {
      //
    }
  }
}
