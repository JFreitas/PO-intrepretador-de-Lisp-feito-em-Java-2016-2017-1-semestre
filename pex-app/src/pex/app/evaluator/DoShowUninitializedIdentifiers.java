/** @version $Id: DoShowUninitializedIdentifiers.java,v 1.3 2016/12/08 20:42:04 ist424781 Exp $ */
package pex.app.evaluator;

import pex.Interpreter;
import pex.operators.Program;


/**
 * Show uninitialized identifiers.
 */
public class DoShowUninitializedIdentifiers extends ProgramCommand {

  /**
   * @param interpreter
   * @param receiver
   */
  public DoShowUninitializedIdentifiers(Interpreter interpreter, Program receiver) {
    super(Label.SHOW_UNINITIALIZED_IDENTIFIERS, interpreter, receiver);
  }

  /** @see pt.tecnico.po.ui.Command#execute() */
  @Override
  public final void execute()  {
    _display.popup(_receiver.unitializedIdentifiers());
  }

}
