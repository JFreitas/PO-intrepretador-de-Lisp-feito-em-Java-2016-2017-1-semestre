/** @version $Id: DoAddExpression.java,v 1.4 2016/12/08 21:44:08 ist424781 Exp $ */
package pex.app.evaluator;

import pex.ParserException;
import pex.Interpreter;
import pex.app.BadExpressionException;
import pex.app.BadPositionException;
import pex.operators.Program;
import pt.tecnico.po.ui.DialogException;
import pt.tecnico.po.ui.Input;


/**
 * Add expression.
 */
public class DoAddExpression extends ProgramCommand {
  /** Input field. */
  Input<Integer> _position;

  /** Input field. */
  Input<String> _description;

  /**
   * @param interpreter
   * @param receiver
   */
  public DoAddExpression(Interpreter interpreter, Program receiver) {
    super(Label.ADD_EXPRESSION, interpreter, receiver);
    _position = _form.addIntegerInput(Message.requestPosition());
    _description = _form.addStringInput(Message.requestExpression());
  }

  /** @see pt.tecnico.po.ui.Command#execute() */
  @Override
  public final void execute() throws DialogException, BadPositionException, BadExpressionException  {
    _form.parse();
    try {
      if (_position.value() >= 0 && _position.value() <= _receiver.size()) {
        _receiver.add(_position.value(), _interpreter.parse(_description.value()));
      }
      else{
        throw new BadPositionException (_position.value());
      }
    }
    catch (ParserException e){
      throw new BadExpressionException (_description.value());
    }
  }
}
