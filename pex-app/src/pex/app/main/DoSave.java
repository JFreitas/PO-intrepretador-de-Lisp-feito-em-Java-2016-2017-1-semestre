/** @version $Id: DoSave.java,v 1.4 2016/11/21 01:37:16 ist424781 Exp $ */
package pex.app.main;

import java.io.IOException;

import pex.Manager;
import pt.tecnico.po.ui.Command;
import pt.tecnico.po.ui.Input;

/**
 * Save to file under current name (if unnamed, query for name).
 */
public class DoSave extends Command<Manager> {
  /** Input field. */
  Input<String> _filename;

  
  /**
   * @param receiver
   */
  public DoSave(Manager receiver) {
    super(Label.SAVE, receiver);
    _filename = _form.addStringInput(Message.newSaveAs());
  }

  /** @see pt.tecnico.po.ui.Command#execute() */
  @Override
  public final void execute() {
    if (_receiver.checkChanges() == true){
      try {
        if (_receiver.getInterpreterName() == null) {
          _form.parse();
          _receiver.setInterpreterName(_filename.value());
        }
        _receiver.saveInterpreter();
        }
      catch (IOException e) {
        System.out.println("Erro : " + _receiver.getInterpreterName() + " : " + e);
      }
    }
  }
}

