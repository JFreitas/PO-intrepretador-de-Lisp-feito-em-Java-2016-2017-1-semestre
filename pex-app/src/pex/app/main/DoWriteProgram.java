/** @version $Id: DoWriteProgram.java,v 1.3 2016/11/21 01:37:16 ist424781 Exp $ */
package pex.app.main;

import java.io.FileNotFoundException;

import pex.operators.Program;
import pex.Manager;
import pt.tecnico.po.ui.Command;
import pt.tecnico.po.ui.Input;
import java.io.IOException;

/**
 * Write (save) program to file.
 */
public class DoWriteProgram extends Command<Manager> {
  /** Input field. */
  Input<String> _programName;
  /** Input field. */
  Input<String> _filename;

  /**
   * @param receiver
   */
  public DoWriteProgram(Manager receiver) {
    super(Label.WRITE_PROGRAM, receiver);
    _programName = _form.addStringInput(Message.requestProgramId());
    _filename = _form.addStringInput(Message.programFileName());
  }

  /** @see pt.tecnico.po.ui.Command#execute() */
  @Override
  public final void execute() {
    _form.parse();
    Program program = _receiver.getProgram(_programName.value());
    try {
      if (program != null) {
        _receiver.writeProgram(_programName.value(), _filename.value());
      } else {
        _display.popup(Message.noSuchProgram(_programName.value()));
      }
    }
    catch (IOException e) {
      System.out.println("Erro : " + _receiver.getInterpreterName() + " : " + e);
    }
  }
}
