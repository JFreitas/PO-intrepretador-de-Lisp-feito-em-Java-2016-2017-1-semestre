/** @version $Id: DoReadProgram.java,v 1.3 2016/11/20 19:12:20 ist424781 Exp $ */
package pex.app.main;

import pex.Manager;
import pex.ParserException;
import pex.app.BadFileException;
import pt.tecnico.po.ui.Command;
import pt.tecnico.po.ui.Input;

/**
 * Read existing program.
 */
public class DoReadProgram extends Command<Manager> {
  /** Input field. */
  Input<String> _filename;

  /**
   * @param receiver
   */
  public DoReadProgram(Manager receiver) {
    super(Label.READ_PROGRAM, receiver);
    _filename = _form.addStringInput(Message.programFileName());
  }

  /** @see pt.tecnico.po.ui.Command#execute() */
  @Override
  public final void execute() throws BadFileException {
    _form.parse();
    try{
      _receiver.readProgram(_filename.value());
    }
    catch (ParserException e){
      throw new BadFileException (_filename.value());
    }
  }

}
