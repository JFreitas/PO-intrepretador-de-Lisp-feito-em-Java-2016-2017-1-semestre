/** @version $Id: DoNew.java,v 1.4 2016/11/21 04:14:51 ist424781 Exp $ */
package pex.app.main;

import java.io.IOException;

import pex.Manager;
import pt.tecnico.po.ui.Command;
import pt.tecnico.po.ui.Input;

/**
 * Open a new interpreter.
 */
public class DoNew extends Command<Manager> {
  /** Input field. */
  Input<Boolean> _shouldSave;

  /**
   * @param receiver
   */
  public DoNew(Manager receiver) {
    super(Label.NEW, receiver);
    _shouldSave = _form.addBooleanInput(Message.saveBeforeExit());
  }

  /** @see pt.tecnico.po.ui.Command#execute() */
  @Override
  public final void execute() {
    _receiver.newInterpreter();
    /*_form.parse();
    try {
      if (_shouldSave.value() == true) {
        _receiver.saveInterpreter();
        _receiver.newInterpreter();
      } else {
        _receiver.newInterpreter();
      }
    }
    catch (IOException e) {
      System.out.println("Erro : " + _receiver.getInterpreterName() + " : " + e);
    }
     */
  }
}
