package pex;

import pex.Parser;
import pex.Interpreter;
import pex.operators.Program;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;

import pex.exceptions.BadExpressionException;
import pex.exceptions.BadNumberException;
import pex.exceptions.ExtraneousDataAtEndOfInputException;
import pex.exceptions.MissingClosingParenthesisException;
import pex.exceptions.EndOfInputException;
import pex.exceptions.UnknownOperationException;

/**
 * class that manages the interpreters and their interactions between the app and the core.
 */
public class Manager {

    /** Parser */
    private Parser _parser = new Parser();

    /** Interpreter */
    private Interpreter _interpreter = new Interpreter();

    /** name of the Interprete used to save in diskr*/
    private String _interpreterName = null;

    /** Flag to check if there was changes so we can save*/
    private boolean _hasChanged = true;


    /**
     *
     * @return checks if there was changes so we can save.
     */
    public boolean checkChanges(){
        return _hasChanged;
    }

    /**
     *
     * @return interpreter in use.
     */
    public Interpreter getInterpreter(){
        return _interpreter;
    }

    /**
     *
     * @return interpreter name.
     */
    public String getInterpreterName(){ return _interpreterName; }

    /**
     *
     * @param name
     */
    public void setInterpreterName(String name){
        _interpreterName = name;
    }

    /**
     * creates and sets a new interpreter
     * */
    public void newInterpreter(){
        _interpreter = new Interpreter();
        _interpreterName = null;
        _hasChanged = true;
    }

    /**
     * save current state of the interpreter.
     *
     * @throws IOException
     */
    public void saveInterpreter()throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(_interpreterName));
        out.writeObject(_interpreter);
        out.close();
        _hasChanged = false;
    }

    /**
     * loads an interpreter through a file.
     *
     * @param file
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void loadInterpreter(String file) throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));

        Interpreter interpreter = (Interpreter) in.readObject();
        in.close();

        _interpreter = interpreter;
        _hasChanged = false;
        _interpreterName = file;

    }

    /**
     * get program, if it does not exist returns null.
     *
     * @param name
     * @return
     */
    public Program getProgram(String name){
        return _interpreter.getProgram(name);
    }

    /**
     * creates a new empty program.
     *
     * @param name
     */
    public void newProgram(String name){
        Program prog = new Program();
        _interpreter.putProgram(name, prog);
        _hasChanged = true;
    }

    /**
     *
     * @param filename
     * @throws ExtraneousDataAtEndOfInputException
     * @throws EndOfInputException
     * @throws MissingClosingParenthesisException
     * @throws UnknownOperationException
     * @throws BadNumberException
     * @throws BadExpressionException
     */
    public void readProgram(String filename)
            throws ExtraneousDataAtEndOfInputException,
            EndOfInputException, MissingClosingParenthesisException, UnknownOperationException,
            BadNumberException, BadExpressionException{
        Program prog = _parser.parseProgramFile(filename);
        _interpreter.putProgram(filename, prog);
        _hasChanged = true;
    }

    /**
     *
     * @param filename
     * @throws ExtraneousDataAtEndOfInputException
     * @throws EndOfInputException
     * @throws MissingClosingParenthesisException
     * @throws UnknownOperationException
     * @throws BadNumberException
     * @throws BadExpressionException
     */
    public void readProgramImport(String filename)
            throws ExtraneousDataAtEndOfInputException,
            EndOfInputException, MissingClosingParenthesisException, UnknownOperationException,
            BadNumberException, BadExpressionException{
        Program prog = _parser.parseProgramFile(filename);
        _interpreter.putProgram("import", prog);
    }

    /**
     *
     * @param programName
     * @param fileName
     * @throws IOException
     */
    public void writeProgram(String programName, String fileName) throws IOException{
        Program program = _interpreter.getProgram(programName);
        PrintWriter out = new PrintWriter(fileName);
        out.print(program.toString());
        out.close();
    }

}

