/* $Id: Identifier.java,v 1.3 2016/11/21 03:58:25 ist424781 Exp $ */
package pex.atomic;

import pex.Expression;
import pex.Value;
import pex.Stringer;
import pex.Visitor;

/**
 * Class for describing syntactic tree leaves for holding identifier values.
 */
public class Identifier extends Expression {

  /** Serial number for serialization. */
  private static final long serialVersionUID = 201608281352L;

  /** Identifier name. */
  private String _name;

  /**
   * @param name
   */
  public Identifier(String name) {
    _name = name;
  }

  /**
   * @return the name
   */
  public String getName() {
    return _name;
  }

  /**
   * function to accept visitors.
   *
   * @param v
   */
  public void accept(Visitor v){
    v.visitIdentifier(this);
  }

  //FIXME (possibly) add other methods: e.g. accept, toString, etc.
}
