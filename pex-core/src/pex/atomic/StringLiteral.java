/* $Id: StringLiteral.java,v 1.3 2016/11/21 03:58:25 ist424781 Exp $ */
package pex.atomic;

import pex.Value;
import pex.Stringer;
import pex.Visitor;

/**
 * Class for describing syntactic tree leaves for holding string values.
 */
public class StringLiteral extends Value<String> {

  /** Serial number for serialization. */
  private static final long serialVersionUID = 201608281352L;

  /**
   * @param value
   */
  public StringLiteral(String value) {
    super(value);
  }

  /**
   * function to accept visitors.
   *
   * @param v
   */
  public void accept(Visitor v){
    v.visitStringLiteral(this);
  }

  //FIXME (possibly) add other methods: e.g. accept, toString, etc.
}
