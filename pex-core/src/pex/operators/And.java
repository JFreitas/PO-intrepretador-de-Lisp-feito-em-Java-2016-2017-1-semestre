/* $Id: And.java,v 1.3 2016/11/21 03:58:25 ist424781 Exp $ */
package pex.operators;

import pex.Expression;
import pex.Stringer;
import pex.Visitor;

/**
 * Class for describing the and operator
 */
public class And extends BinaryExpression {

  /** Serial number for serialization. */
  private static final long serialVersionUID = 201608281352L;

  /**
   * @param first
   * @param second
   */
  public And(Expression first, Expression second) {
    super(first, second);
  }

  /**
   * function to accept visitors.
   *
   * @param v
   */
  public void accept(Visitor v){
    v.visitAnd(this);
  }
  //FIXME (possibly) add other methods: e.g. accept, toString, etc.
}
