/* $Id: Le.java,v 1.3 2016/11/21 03:58:25 ist424781 Exp $ */
package pex.operators;

import pex.Expression;
import pex.Stringer;
import pex.Visitor;

/**
 * Class for describing the lesser or equal ('<=') operator
 */
public class Le extends BinaryExpression {

  /** Serial number for serialization. */
  private static final long serialVersionUID = 201608281352L;

  /**
   * @param first
   * @param second
   */
  public Le(Expression first, Expression second) {
    super(first, second);
  }

  /**
   * function to accept visitors.
   *
   * @param v
   */
  public void accept(Visitor v){
    v.visitLe(this);
  }
  //FIXME (possibly) add other methods: e.g. accept, toString, etc.
}
