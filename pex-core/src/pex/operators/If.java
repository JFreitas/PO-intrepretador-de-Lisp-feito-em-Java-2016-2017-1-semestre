package pex.operators;

import pex.Expression;
import pex.Stringer;
import pex.Visitor;

/**
 * Class for describing the if function
 */
public class If extends Expression{
	private static final long serialVersionUID = 201611101352L;

	/** First operand. */
  	Expression _first;

  	/** Second operand. */
  	Expression _second;

  	/** Third operand. */
  	Expression _third;  	

  	public If (Expression first, Expression second, Expression third){
  		_first = first;
  		_second = second;
  		_third = third;
  	}

	public Expression first(){
		return _first;
	}

	public Expression second(){
		return _second;
	}

	public Expression third() {return _third; }

	/**
	 * function to accept visitors.
	 *
	 * @param v
	 */
	public void accept(Visitor v){
		v.visitIf(this);
	}
}