/* $Id: Neg.java,v 1.3 2016/11/21 03:58:25 ist424781 Exp $ */
package pex.operators;

import pex.Expression;
import pex.Stringer;
import pex.Visitor;

/**
 * Class for describing the negative ('-') operator
 */
public class Neg extends UnaryExpression {

  /** Serial number for serialization. */
  private static final long serialVersionUID = 201608281352L;

  /**
   * @param argument
   */
  public Neg(Expression argument) {
    super(argument);
  }

  //FIXME (possibly) add other methods: e.g. accept, toString, etc.

  /**
   * function to accept visitors.
   *
   * @param v
   */
  public void accept(Visitor v){
    v.visitNeg(this);
  }

}
