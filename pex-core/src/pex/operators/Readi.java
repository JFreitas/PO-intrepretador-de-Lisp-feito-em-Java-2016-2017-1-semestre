package pex.operators;

import pex.Expression;
import pex.Stringer;
import pex.Visitor;

/**
 * Class for introducing an integer
 */
public class Readi extends Expression {

  /** Serial number for serialization. */
  private static final long serialVersionUID = 201611101352L;

  /**
   * function to accept visitors.
   *
   * @param v
   */
  public void accept(Visitor v){
    v.visitReadi(this);
  }

  //FIXME (possibly) add other methods: e.g. accept, toString, etc.
}
