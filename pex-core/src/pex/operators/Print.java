package pex.operators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pex.Expression;
import pex.Stringer;
import pex.Visitor;

/**
 * Class representing a sequence of expressions to be printed.
 */
public class Print extends Sequence {

	/** Serial number for serialization. */
	private static final long serialVersionUID = 201611101352L;

	/**
	 * Default constructor (empty print).
	 */
	public Print (){ super(); }

	/**
	 * Constructor for single-expression prints.
	 *
	 * @param expression
	 */
 	public Print(Expression expression) {
 		super(expression);
  	}

	/**
	 * Constructor for appending expressions to prints.
	 *
	 * @param previous
	 * @param expression
	 */
  	public Print(Sequence previous, Expression expression) {
  		super(previous, expression);
	}

	/**
	 * @param expressions
	 */
	public Print(List<Expression> expressions) {
		super(expressions);
	}

	/**
	 * function to accept visitors.
	 *
	 * @param v
	 */
	public void accept(Visitor v){
		v.visitPrint(this);
	}

}