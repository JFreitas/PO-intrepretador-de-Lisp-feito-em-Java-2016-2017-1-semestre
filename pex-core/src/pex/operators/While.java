/* $Id: While.java,v 1.4 2016/12/09 08:21:58 ist424781 Exp $ */
package pex.operators;

import pex.Expression;
import pex.Stringer;
import pex.Visitor;

/**
 * Class for describing the while function
 */
public class While extends Expression {

  /** Serial number for serialization. */
  private static final long serialVersionUID = 201608281352L;

  /** First operand. */
  Expression _first;

  /** Second operand. */
  Expression _second;

  public While(Expression first, Expression second) {
    _first = first;
    _second = second;
  }

  /**
   * @return first operand
   */
  public Expression first() {
    return _first;
  }

  /**
   * @return second operand
   */
  public Expression second() {
    return _second;
  }

  /**
   * function to accept visitors.
   *
   * @param v
   */
  public void accept(Visitor v){
    v.visitWhile(this);
  }

  //FIXME (possibly) add other methods: e.g. accept, toString, etc.
}
