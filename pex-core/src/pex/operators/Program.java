/* $Id: Program.java,v 1.6 2016/12/09 08:21:58 ist424781 Exp $ */
package pex.operators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pex.Expression;
import pex.Stringer;
import pex.Visitor;
import pex.AllIdentifiersVisitor;
import pex.UnitializedIdentifiersVisitor;

/**
 * Class for describing programs.
 */
public class Program extends Sequence{

	/** Serial number for serialization. */
	private static final long serialVersionUID = 201611101352L;

	/**
	 * Default constructor (empty program).
	 */
	public Program (){ super(); }

	/**
	 * Constructor for single-expression programs.
	 *
	 * @param expression
	 */
	public Program (Expression expression) {
		super(expression);
	}

	/**
	 * Constructor for appending expressions to programs.
	 *
	 * @param previous
	 * @param expression
	 */
	public Program (Sequence previous, Expression expression) {
		super(previous, expression);
	}

	/**
	 * @param expressions
	 */
	public Program (List<Expression> expressions) {
		super(expressions);
	}

	/**
	 * function to accept visitors.
	 *
	 * @param v
	 */
	public void accept(Visitor v){
		v.visitProgram(this);
	}

	/**
	 * @return string to show program or to write program.
	 */
	@Override
	public String toString(){
		Stringer stringer = new Stringer();
		this.accept(stringer);
		return stringer.toString();
	}

	public String allIdentifiers(){
		AllIdentifiersVisitor allIdVisitor = new AllIdentifiersVisitor();
		this.accept(allIdVisitor);
		return allIdVisitor.toString();
	}

	public String unitializedIdentifiers(){
		UnitializedIdentifiersVisitor unIdVisitor = new UnitializedIdentifiersVisitor();
		this.accept(unIdVisitor);
		return unIdVisitor.toString();
	}

}
