/* $Id: Lt.java,v 1.3 2016/11/21 03:58:25 ist424781 Exp $ */
package pex.operators;

import pex.Expression;
import pex.Stringer;
import pex.Visitor;

/**
 * Class for describing the lesser than ('<') operator
 */
public class Lt extends BinaryExpression {

  /** Serial number for serialization. */
  private static final long serialVersionUID = 201608281352L;

  /**
   * @param first
   * @param second
   */
  public Lt(Expression first, Expression second) {
    super(first, second);
  }

  public void accept(Visitor v){
    v.visitLt(this);
  }

  //FIXME (possibly) add other methods: e.g. accept, toString, etc.
}
