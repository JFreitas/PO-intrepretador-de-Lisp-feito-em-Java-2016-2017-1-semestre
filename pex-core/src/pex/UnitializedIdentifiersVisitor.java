package pex;

import pex.atomic.Identifier;
import pex.atomic.IntegerLiteral;
import pex.atomic.StringLiteral;
import pex.operators.Add;
import pex.operators.Mul;
import pex.operators.Not;
import pex.operators.Neg;
import pex.operators.Sub;
import pex.operators.Div;
import pex.operators.Mod;
import pex.operators.Lt;
import pex.operators.Le;
import pex.operators.Ge;
import pex.operators.Gt;
import pex.operators.Eq;
import pex.operators.Ne;
import pex.operators.And;
import pex.operators.Or;
import pex.operators.Set;
import pex.operators.Print;
import pex.operators.Readi;
import pex.operators.Reads;
import pex.operators.If;
import pex.operators.While;
import pex.operators.Call;
import pex.operators.Program;
import pex.operators.Sequence;

import java.util.TreeSet;
import java.util.Collections;
import java.util.List;

public class UnitializedIdentifiersVisitor implements Visitor {
    private TreeSet<String> _identifiers = new TreeSet<String>();
    private TreeSet<String> _initIdentifiers = new TreeSet<String>();
    private boolean _isInitialized = false;

    public void visitIdentifier(Identifier i) {
        if(_isInitialized){
            _identifiers.add(i.getName());
            _initIdentifiers.add(i.getName());
            _isInitialized = false;
        }
        else {
            _identifiers.add(i.getName());
        }
    }

    public void visitSet(Set e){
        if(e.first() instanceof Identifier){
            _isInitialized = true;
        }
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitIntegerLiteral(IntegerLiteral i) {}
    public void visitStringLiteral(StringLiteral s) {}

    public void visitAdd(Add e){
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitMul(Mul e){
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitNot(Not e){
        e.argument().accept(this);
    }

    public void visitNeg(Neg e){
        e.argument().accept(this);
    }

    public void visitSub(Sub e){
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitDiv(Div e){
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitMod(Mod e){
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitLt(Lt e){
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitLe(Le e){
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitGe(Ge e){
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitGt(Gt e){
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitEq(Eq e){
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitNe(Ne e){
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitAnd(And e){
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitOr(Or e){
        e.first().accept(this);
        e.second().accept(this);
    }



    public void visitPrint(Print p){
        List<Expression> expressions = p.getAll();
        for(Expression e: expressions){
            e.accept(this);
        }
    }

    public void visitReadi(Readi r){}

    public void visitReads(Reads r){}

    public void visitIf(If i){
        i.first().accept(this);
        i.second().accept(this);
        i.third().accept(this);
    }

    public void visitWhile(While e){
        e.first().accept(this);
        e.second().accept(this);
    }

    public void visitCall(Call e){
        e.argument().accept(this);
    }

    public void visitProgram(Program p){
        List<Expression> expressions = p.getAll();
        for(Expression e: expressions){
            e.accept(this);
        }

    }
    public void visitSequence(Sequence p){
        List<Expression> expressions = p.getAll();
        for(Expression e: expressions){
            e.accept(this);
        }
    }

    public String toString(){
        for(String s1: _initIdentifiers){
            _identifiers.remove(s1);
        }
        String value = "";
        for(String s2: _identifiers){
            value += s2 + "\n";
        }
        return value;
    }
}