package pex;

import java.util.HashMap;
import pex.operators.Program;
import java.io.Serializable;
import pex.Parser;
import pex.ParserException;

/**
 * class that holds a colection of programs and manages them.
 */
public class Interpreter implements Serializable{

    /** Serial number for serialization. */
    private static final long serialVersionUID = 201611101352L;

    /**
     * Storage for Programs.
     */
    private HashMap<String, Program> _programMap = new HashMap<String, Program>();

    /**
     * Instance of Parser
    **/
    private Parser _parser = new Parser();

    /**
     *
     * @param s
     * @param p
     */
    public void putProgram(String s, Program p){
        _programMap.put(s, p);
    }

    /**
     *
     * @param s
     * @return
     */
    public Program getProgram(String s){
        return _programMap.get(s);
    }

    public Expression parse(String s) throws ParserException{
        return _parser.parse(s);
    }
}