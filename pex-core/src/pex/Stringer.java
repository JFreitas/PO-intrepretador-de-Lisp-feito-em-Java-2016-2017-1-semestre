package pex;

import pex.atomic.Identifier;
import pex.atomic.IntegerLiteral;
import pex.atomic.StringLiteral;
import pex.operators.Add;
import pex.operators.Mul;
import pex.operators.Not;
import pex.operators.Sub;
import pex.operators.Div;
import pex.operators.Mod;
import pex.operators.Neg;
import pex.operators.Lt;
import pex.operators.Le;
import pex.operators.Ge;
import pex.operators.Gt;
import pex.operators.Eq;
import pex.operators.Ne;
import pex.operators.And;
import pex.operators.Or;
import pex.operators.Set;
import pex.operators.Print;
import pex.operators.Readi;
import pex.operators.Reads;
import pex.operators.If;
import pex.operators.While;
import pex.operators.Call;
import pex.operators.Program;
import pex.operators.Sequence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import pex.Expression;

public class Stringer implements Visitor{
    private String _string = "";

    @Override
    public String toString(){
        return _string;
    }

    public void visitIdentifier(Identifier i){
        _string+= i.getName();
    }

    public void visitIntegerLiteral(IntegerLiteral i){
        _string+= i.toString();
    }

    public void visitStringLiteral(StringLiteral s){
        _string+= "\"";
        _string+= s.toString();
        _string+= "\"";
    }

    public void visitAdd(Add e){
        _string+= "(add ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitMul(Mul e){
        _string+= "(mul ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitNot(Not e){
        _string+= "(not ";
        e.argument().accept(this);
        _string+= ")";
    }

    public void visitNeg(Neg e){
        _string+= "(neg ";
        e.argument().accept(this);
        _string+= ")";
    }

    public void visitSub(Sub e){
        _string+= "(sub ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitDiv(Div e){
        _string+= "(div ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitMod(Mod e){
        _string+= "(mod ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitLt(Lt e){
        _string+= "(lt ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitLe(Le e){
        _string+= "(le ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitGe(Ge e){
        _string+= "(ge ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitGt(Gt e){
        _string+= "(gt ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitEq(Eq e){
        _string+= "(eq ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitNe(Ne e){
        _string+= "(ne ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitAnd(And e){
        _string+= "(and ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitOr(Or e){
        _string+= "(or ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitSet(Set e){
        _string+= "(set ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitPrint(Print p){
        List<Expression> expressions = p.getAll();
        _string+= "(print";
        for(Expression e: expressions){
            _string+=" ";
            e.accept(this);
        }
        _string+= ")";
    }

    public void visitReadi(Readi r){
        _string+= "(readi)";
    }

    public void visitReads(Reads r){
        _string+= "(reads)";
    }

    public void visitIf(If i){
        _string+="(if ";
        i.first().accept(this);
        _string+= " " ;
        i.second().accept(this);
        _string+= " ";
        i.third().accept(this);
        _string+= ")";
    }

    public void visitWhile(While e){
        _string+= "(while ";
        e.first().accept(this);
        _string+= " ";
        e.second().accept(this);
        _string+= ")";
    }

    public void visitCall(Call e){
        _string+= "(call ";
        e.argument().accept(this);
        _string+= ")";
    }

    public void visitProgram(Program p){
        List<Expression> expressions = p.getAll();
        for(Expression e: expressions){
            e.accept(this);
            _string+= "\n";
        }

    }
    public void visitSequence(Sequence p){
        List<Expression> expressions = p.getAll();
        _string+= "(seq";
        for(Expression e: expressions){
            _string+=" " ;
            e.accept(this);
        }
        _string+= ")";
    }
}