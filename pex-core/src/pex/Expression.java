/* $Id: Expression.java,v 1.3 2016/11/21 03:58:25 ist424781 Exp $ */
package pex;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.Serializable;

/**
 * An expressions can be evaluated to produce a value.
 */
public abstract class Expression implements Serializable {

  /** Serial number for serialization. */
  private static final long serialVersionUID = 201608281352L;

  /**
   * function to accept visitors.
   *
   * @param v
   */
  public abstract void accept(Visitor v);
  
  //FIXME (possibly) add other methods: e.g. accept, toString, etc.

}
